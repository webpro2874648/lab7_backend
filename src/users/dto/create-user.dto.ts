import {IsEmail, IsNotEmpty, Length, IsArray} from 'class-validator';
export class CreateUserDto {
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @Length(4, 32)
  password: string;
  
  @IsNotEmpty()
  fullName: string;

  // @IsArray()
  // roles: ('admin' | 'user')[];

  @IsNotEmpty()
  gender: 'male' | 'female' | 'others';
}
