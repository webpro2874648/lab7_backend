import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  Name: string;
  
  @Column()
  StartDate: string;
  
  @Column()
  EndDate: string;
  
  @Column()
  Detail: string;
  // roles: ('admin' | 'user')[];
  // @Column()
  // gender: 'male' | 'female' | 'others';
}
