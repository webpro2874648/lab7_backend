import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Promotion } from './entities/promotion.entity';

@Injectable()
export class PromotionsService {
  constructor(
    @InjectRepository(Promotion)
    private promotionsRepository: Repository<Promotion>,
  ) {}

  create(createPromotionDto: CreatePromotionDto): Promise<Promotion> {
    return this.promotionsRepository.save(createPromotionDto);
  }

  findAll(): Promise<Promotion[]> {
    return this.promotionsRepository.find();
  }

  findOne(id: number) {
    return this.promotionsRepository.findOneBy({ id: id });
  }

  async update(id: number, updatePromotionDto: UpdatePromotionDto) {
    await this.promotionsRepository.update(id, updatePromotionDto);
    const promotion = await this.promotionsRepository.findOneBy({ id });
    return promotion;
  }

  async remove(id: number) {
    const deletePromotion = await this.promotionsRepository.findOneBy({ id });
    return this.promotionsRepository.remove(deletePromotion);
  }
}
