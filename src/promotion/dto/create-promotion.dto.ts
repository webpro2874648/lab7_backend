import { IsNotEmpty } from 'class-validator';
export class CreatePromotionDto {
  @IsNotEmpty()
  Name: string;

  @IsNotEmpty()
  StartDate: string;

  @IsNotEmpty()
  EndDate: string;

  @IsNotEmpty()
  Detail: string;
  
  // @IsNotEmpty()
  // fullName: string;

  // // @IsArray()
  // // roles: ('admin' | 'user')[];

  // @IsNotEmpty()
  // gender: 'male' | 'female' | 'others';
}
